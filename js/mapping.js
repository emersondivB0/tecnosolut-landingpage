// Objeto para almacenar elementos marcados
const elementosMarcadosScript1 = {
    elementos: [],
    arbeit: []
};

// Obtener todos los elementos con clase "elemento-padre" y "elemento-hijo" para el primer script
const elementosPadreScript1 = document.querySelectorAll('.elemento-padre');
const elementosHijoScript1 = document.querySelectorAll('.elemento-hijo');

elementosPadreScript1.forEach(elementoPadreScript1 => {
    elementoPadreScript1.addEventListener('change', function () {
        // Si se marca o desmarca un elemento padre,
        // seleccionar o deseleccionar automáticamente todos los elementos hijos correspondientes
        elementosHijoScript1.forEach(elementoHijoScript1 => {
            if (elementoHijoScript1.parentElement === this) {
                elementoHijoScript1.checked = this.checked;
            }
        });

        // Actualizar la lista de elementos marcados según el estado de los elementos hijos
        elementosMarcadosScript1.elementos = [];
        elementosMarcadosScript1.arbeit = [];
        elementosHijoScript1.forEach(elementoHijoScript1 => {
            if (elementoHijoScript1.checked) {
                elementosMarcadosScript1.elementos.push({
                    id: elementoHijoScript1.id,
                    tipo: 'hijo'
                });
                const arbeit = elementoHijoScript1.getAttribute('data-arbeit');

                elementosMarcadosScript1.arbeit.push(arbeit);
            }
        });

        // Imprimir la lista al final del evento de cambio
        imprimirListaScript1();
    });
});

elementosHijoScript1.forEach(elementoHijoScript1 => {
    elementoHijoScript1.addEventListener('change', function () {
        // Si se marca o desmarca un elemento hijo,
        // actualizar la lista de elementos marcados
        elementosMarcadosScript1.elementos = [];
        elementosHijoScript1.forEach(elementoHijoScript1 => {
            if (elementoHijoScript1.checked) {
                elementosMarcadosScript1.elementos.push({
                    id: elementoHijoScript1.id,
                    tipo: 'hijo'
                });
                const arbeit = elementoHijoScript1.getAttribute('data-arbeit');

                elementosMarcadosScript1.arbeit.push(arbeit);
            }
        });

        // Imprimir la lista al final del evento de cambio
        imprimirListaScript1();
    });
});

// Imprimir la lista en la consola del primer script
function imprimirListaScript1() {
    console.log('Elementos seleccionados con precio y horas:');
    elementosMarcadosScript1.elementos.forEach(item => {
        console.log(`ID: ${item.id}, Horas: ${elementosMarcadosScript1.arbeit[elementosMarcadosScript1.elementos.indexOf(item)]}`);
    });
    // Calcular el total de horas
    let totalHoras = 0;
    elementosMarcadosScript1.elementos.forEach(item => {
        totalHoras = totalHoras + Number(elementosMarcadosScript1.arbeit[elementosMarcadosScript1.elementos.indexOf(item)]);
    });
    console.log(`Total de horas: ${totalHoras}, Monto total: $ ${(totalHoras * 50) * 1.3}`);
}

// Agregar un evento de cambio a los botones de selección
const botonSeleccionarTodos = document.querySelector('button[onclick="seleccionarTodos()"]');
const botonDeseleccionarTodos = document.querySelector('button[onclick="deseleccionarTodos()"]');

botonSeleccionarTodos.addEventListener('click', function () {
    // Actualizar la lista de elementos marcados
    actualizarLista();
});

botonDeseleccionarTodos.addEventListener('click', function () {
    // Actualizar la lista de elementos marcados
    actualizarLista();
});

// Función para actualizar la lista de elementos marcados
function actualizarLista() {
    // Obtener todos los elementos con clase "elemento-hijo"
    const elementosHijo = document.querySelectorAll('.elemento-hijo');

    // Vaciar la lista de elementos marcados
    elementosMarcadosScript1.elementos = [];
    elementosMarcadosScript1.arbeit = [];

    // Agregar todos los elementos hijos a la lista de elementos marcados
    elementosHijo.forEach(elementoHijo => {
        if (elementoHijo.checked) {
            elementosMarcadosScript1.elementos.push({
                id: elementoHijo.id,
                tipo: 'hijo'
            });
            const arbeit = elementoHijo.getAttribute('data-arbeit');

            elementosMarcadosScript1.arbeit.push(arbeit);
        }
    });

    // Imprimir la lista en la consola
    imprimirListaScript1();
}

// Monto en pantalla
function calcular() {
    // Aquí puedes realizar tus cálculos y obtener el número o cadena deseado
    let totalArbite = 0;
    elementosMarcadosScript1.elementos.forEach(item => {
        totalArbite = totalArbite + Number(elementosMarcadosScript1.arbeit[elementosMarcadosScript1.elementos.indexOf(item)]);
    });

    // Luego, asigna el resultado al cuadro de texto no editable
    document.getElementById("resultado").value = Math.ceil((totalArbite * 50) * 1.3);
}
// Email send handler form 
//
(function () {
    // get all data in form and return object
    function getFormData(form) {
        var elements = form.elements;
        var formData = {};
        var honeypot;

        var fields = Object.keys(elements).filter(function (k) {
            if (elements[k].name === "honeypot") {
                honeypot = elements[k].value;
                return false;
            }
            return true;
        }).map(function (k) {
            if (elements[k].name !== undefined) {
                return elements[k].name;
                // special case for Edge's html collection
            } else if (elements[k].length > 0) {
                return elements[k].item(0).name;
            }
        }).filter(function (item, pos, self) {
            return self.indexOf(item) == pos && item;
        });

        var formData = {};
        fields.forEach(function (name) {
            var element = elements[name];
            const totalArbite = calcular();
            // Add the listaElementosMarcados from mapping.js
            var listaElementosMarcados = elementosMarcadosScript1;
            listaElementosMarcados.totalArbite = totalArbite;
            formData.listaElementosMarcados = JSON.stringify(listaElementosMarcados);

            // singular form elements just have one value
            formData[name] = element.value;

            // when our element has multiple items, get their values
            if (element.length) {
                var data = [];
                for (var i = 0; i < element.length; i++) {
                    var item = element.item(i);
                    if (item.checked || item.selected) {
                        data.push(item.value);
                    }
                }
                formData[name] = data.join(', ');
            }
        });

        // add form-specific values into the data
        formData.formDataNameOrder = JSON.stringify(fields);
        formData.formGoogleSheetName = form.dataset.sheet || "responses"; // default sheet name
        formData.formGoogleSendEmail
            = form.dataset.email || ""; // no email by default
        console.log(formData);

        return { data: formData, honeypot: honeypot };
    }

    function handleFormSubmit(event) {  // handles form submit without any jquery
        event.preventDefault();           // we are submitting via xhr below
        var form = event.target;
        // Validate the form 
        if (!validateForm()) {
            return false;
        }
        var formData = getFormData(form);
        var data = formData.data;

        // If a honeypot field is filled, assume it was done so by a spam bot.
        if (formData.honeypot) {
            return false;
        }

        disableAllButtons(form);
        var url = form.action;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        // xhr.withCredentials = true;
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                form.reset();
                var formElements = form.querySelector(".form-elements")
                if (formElements) {
                    formElements.style.display = "none"; // hide form
                }
                var thankYouMessage = form.querySelector(".thankyou_message");
                if (thankYouMessage) {
                    thankYouMessage.style.display = "block";
                }
            }
        };
        // url encode form data for sending as post data
        var encoded = Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
        }).join('&');
        xhr.send(encoded);
    }

    function loaded() {
        // bind to the submit event of our form
        var forms = document.querySelectorAll("form.gform");
        for (var i = 0; i < forms.length; i++) {
            forms[i].addEventListener("submit", handleFormSubmit, false);
        }
    };
    document.addEventListener("DOMContentLoaded", loaded, false);

    function disableAllButtons(form) {
        var buttons = form.querySelectorAll("button");
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].disabled = true;
        }
    }
})();

// Validate Form Function 

function validateForm() {
  // Obtener los valores de los campos del formulario
  const name = document.querySelector('input[name="name"]').value;
  const email = document.querySelector('input[name="email"]').value;
  const phone = document.querySelector('input[name="phone"]').value;

  // Validar los campos del formulario
  if (name === '') {
    alert('El campo "Nombre" es obligatorio.');
    return false;
  }

  if (email === '') {
    alert('El campo "Correo electrónico" es obligatorio.');
    return false;
  }

  // Validar el formato del correo electrónico
  const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if (!regexEmail.test(email)) {
    alert('El campo "Correo electrónico" debe tener un formato válido.');
    return false;
  }

  if (phone === '') {
    alert('El campo "Teléfono" es obligatorio.');
    return false;
  }
  // Regular expression to match a valid phone number format
  var phoneRegex = /^\d{10}$|^\+\d{1,3}\s?\(\d{1,4}\)\s?\d{6,10}$|^\d{1,4}\s?\d{6,10}$|^\+\d{1,3}\s?\d{1,4}\s?\d{6,10}$/;


  if (!phoneRegex.test(phone)) {
    alert('El formato del número de teléfono no es válido.');
    return false;
  }


  // Si todos los campos son válidos, devolver true para enviar el formulario
  return true;
}
