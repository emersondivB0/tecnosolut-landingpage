// Función para actualizar la lista de elementos marcados, precios y horas
function actualizarLista() {
    // Obtener todos los elementos con clase "elemento-hijo"
    const elementosHijo = document.querySelectorAll('.elemento-hijo');

    // Inicializar la lista de elementos marcados, precios y horas
    const elementosSeleccionados = [];

    // Recorrer los elementos hijos
    elementosHijo.forEach(elementoHijo => {
        if (elementoHijo.checked) {
            const id = elementoHijo.id;
            const precio = elementoHijo.getAttribute('data-precio');
            const horas = elementoHijo.getAttribute('data-horas');

            elementosSeleccionados.push({
                id,
                precio,
                horas,
                tipo: 'hijo'
            });
        }
    });

    // Imprimir la lista en la consola
    imprimirListaConPrecioYHoras(elementosSeleccionados);
}

// Función para imprimir la lista con precios y horas
function imprimirListaConPrecioYHoras(lista) {
    console.log('Elementos seleccionados con precio y horas:');
    lista.forEach(item => {
        console.log(`ID: ${item.id}, Precio: ${item.precio}, Horas: ${item.horas}`);
    });
}

// Asociar eventos de cambio a los botones de selección
botonSeleccionarTodos.addEventListener('click', function () {
    // Seleccionar todos los elementos hijos
    elementosHijoScript1.forEach(elementoHijo => {
        elementoHijo.checked = true;
    });

    // Actualizar la lista de elementos marcados, precios y horas
    actualizarLista();
});

botonDeseleccionarTodos.addEventListener('click', function () {
    // Deseleccionar todos los elementos hijos
    elementosHijoScript1.forEach(elementoHijo => {
        elementoHijo.checked = false;
    });

    // Actualizar la lista de elementos marcados, precios y horas
    actualizarLista();
});

