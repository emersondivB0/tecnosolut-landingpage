function seleccionarTodos() {
    var elementos = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].checked = true;
    }
}

function deseleccionarTodos() {
    var elementos = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < elementos.length; i++) {
        elementos[i].checked = false;
    }
}

// Seleccionar todos los elementos con clase "elemento-padre"
const elementosPadre = document.querySelectorAll('.elemento-padre');

// Agregar un evento de cambio (cambio de checkbox) a cada elemento padre
elementosPadre.forEach(elementoPadre => {
    elementoPadre.addEventListener('change', function () {
        // Obtener la lista de elementos hijos asociados al elemento padre
        const elementosHijo = this.parentElement.querySelectorAll('.elemento-hijo');

        // Marcar o desmarcar los elementos hijos según el estado del elemento padre
        elementosHijo.forEach(elementoHijo => {
            elementoHijo.checked = this.checked;
        });
    });
});
