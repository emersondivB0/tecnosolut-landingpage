// Objeto para almacenar elementos marcados
const elementosMarcadosScript1 = {
    elementos: []
};

// Obtén todos los elementos con clase "elemento-padre" para el primer script
const elementosPadreScript1 = document.querySelectorAll('.elemento-padre');
const elementosHijoScript1 = document.querySelectorAll('.elemento-hijo');

// Agrega un evento de cambio (cambio de checkbox) a cada elemento padre del primer script
elementosPadreScript1.forEach(elementoPadreScript1 => {
    elementoPadreScript1.addEventListener('change', function () {
        // Si se marca, agrega el elemento padre a la lista anidada del primer script
        if (this.checked) {
            elementosMarcadosScript1.elementos.push({
                id: this.id,
                tipo: 'padre'
            });
            console.log("se ejecuta (padre)");
        } else {
            // Si se desmarca, elimina el elemento padre de la lista anidada del primer script
            const index = elementosMarcadosScript1.elementos.findIndex(elemento => elemento.id === this.id);
            if (index !== -1) {
                elementosMarcadosScript1.elementos.splice(index, 1);
            }
        }
        // Imprime la lista al final del evento de cambio
        imprimirListaScript1();
    });
});

elementosHijoScript1.forEach(elementoHijoScript1 => {
    elementoHijoScript1.addEventListener('change', function () {
        // Si se marca, agrega el elemento hijo a la lista anidada
        if (this.checked) {
            elementosMarcadosScript1.elementos.push({
                id: this.id,
                tipo: 'hijo'
            });
            console.log("se ejecuta (hijo)");
        } else {
            // Si se desmarca, elimina el elemento hijo de la lista anidada
            const index = elementosMarcadosScript1.elementos.findIndex(elemento => elemento.id === this.id);
            if (index !== -1) {
                elementosMarcadosScript1.elementos.splice(index, 1);
            }
        }
        imprimirListaScript1(); // Llamar a la función para imprimir la lista
    });
});

// Función para imprimir la lista en la consola del primer script
function imprimirListaScript1() {
    console.log('Elementos marcados (script 1):', elementosMarcadosScript1.elementos);
}
