    const openModalButton = document.getElementById('openModal');
    const closeModalButton = document.getElementById('closeModal');
    const modal = document.getElementById('modal');

    openModalButton.addEventListener('click', () => {
        modal.style.display = 'block';
    });

    closeModalButton.addEventListener('click', () => {
        modal.style.display = 'none';
    });

    window.addEventListener('click', (e) => {
        if (e.target === modal) {
            modal.style.display = 'none';
        }
    });

    function handleFormSubmit(event) {
  event.preventDefault();
  var form = event.target;
  
  // Validate the form
  if (!validateForm()) {
    return false;
  }
  
  // Proceed with form submission if validation passes
  var formData = getFormData(form);
  var data = formData.data;
  
  // Rest of the code for form submission...
}
